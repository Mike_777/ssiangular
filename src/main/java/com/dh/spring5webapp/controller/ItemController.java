/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dh.spring5webapp.repositories.ItemRepository;

@Controller
public class ItemController {
    private ItemRepository itemRepository;

    @Autowired
    private ItemRepository itemService;

    public ItemController(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @RequestMapping("/items")
    public String getItems(Model model) {
        model.addAttribute("items", itemRepository.findAll());
        return "items";
    }

    @RequestMapping("/items/{id}")
    public String getItem(Model model, @PathVariable String id) {
        model.addAttribute("item", itemService.findById(Long.valueOf(id)).get());
        return "item";
    }

}
